import PropTypes from 'prop-types';
import DropdownInput from './DropdownInput';

const DropdownLabeled = function DropdownLabeled({ label, handleChange, name }) {
  return (
    <div>
      <p className="mb-2 font-relaway italic text-gray-700">{label}</p>
      <DropdownInput handleChange={handleChange} name={name} />
    </div>
  );
};

DropdownLabeled.propTypes = {
  label: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export default DropdownLabeled;
