/* eslint-disable object-curly-newline */
import PropTypes from 'prop-types';

const InputTextArea = function InputTextArea({ label, name, className, handleChange }) {
  return (
    <div className={className}>
      <p className="mb-2 font-relaway italic text-gray-700">{`${label}:`}</p>
      <textarea
        className="w-full p-4 text-md font-relaway italic border-1 border-gray-200 accent-orange-400"
        name={name}
        rows="3"
        columns="30"
        onChange={handleChange}
      />
    </div>
  );
};

InputTextArea.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default InputTextArea;
