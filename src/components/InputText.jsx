/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-one-expression-per-line */
import PropTypes from 'prop-types';

const InputText = function InputText({ label, name, className, handleChange }) {
  return (
    <div className={className}>
      <p className="mb-2 font-relaway italic text-gray-700">{label}:</p>
      <input
        type="text"
        name={name}
        className="w-full h-14 px-4 text-md font-relaway italic border-1 border-gray-200 accent-orange-400"
        onChange={handleChange}
      />
    </div>
  );
};

InputText.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
};

InputText.defaultProps = {
  className: '',
};

export default InputText;
