/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState } from 'react';
import PropTypes from 'prop-types';

const DropdownInput = function DropdownInput({ handleChange, name }) {
  const [selectedNumber, setSelectedNumber] = useState(0);
  const [hovered, setHovered] = useState(false);
  const [ulHovered, setUlHovered] = useState(false);

  const handleClick = (e) => {
    e.preventDefault();
    e.target.name = name;
    e.target.value = Number(e.target.innerText);
    handleChange(e);
    setSelectedNumber(Number(e.target.innerText));
    setHovered(false);
    setUlHovered(false);
  };

  return (
    <div className="w-full">
      <div className="dropdown inline-block relative w-full">
        <button
          type="button"
          className="bg-white text-center w-full border-1 border-gray-200 text-gray-900 py-2 h-14 px-4 rounded inline-flex justify-between items-center"
          onMouseEnter={() => setHovered(true)}
          onMouseLeave={() => setHovered(false)}
        >
          <span className="mr-1 w-full">
            {selectedNumber === 0 ? 'Numero de Invitados' : selectedNumber}
          </span>
          <svg
            className="fill-current h-4 w-4 float-right"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />{' '}
          </svg>
        </button>
        <ul
          className="w-full absolute hidden text-gray-700 pt-1 border-1 border-gray-200"
          onMouseEnter={() => setUlHovered(true)}
          onMouseLeave={() => setUlHovered(false)}
          style={{
            display: hovered || ulHovered ? 'block' : 'none',
          }}
        >
          <li className="text-center">
            <a
              className="rounded-t bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              1
            </a>
          </li>
          <li className="text-center">
            <a
              className="bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              2
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              3
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              4
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              5
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              6
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              7
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              8
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              9
            </a>
          </li>
          <li className="text-center">
            <a
              className="rounded-b bg-white hover:bg-gray-100 py-2 px-4 block"
              href="#"
              onClick={handleClick}
            >
              10
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

DropdownInput.propTypes = {
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export default DropdownInput;
