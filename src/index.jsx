import React from 'react';
import ReactDOM from 'react-dom/client';
//* Router
import { BrowserRouter } from 'react-router-dom';
//* Main Component
import './index.css';
import App from './App';

//* SubDomain Routing
/* const parsedData = window.location.host.split(".");

if(parsedData.length >= 3){
    const subDomain = parsedData[0];
    ReactDOM.render(<SubDomainApp subDomain={subDomain} />, document.getElementById('root'));
}else{
    ReactDOM.render(<MainApp />, document.getElementById('root'));
} */

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
);
