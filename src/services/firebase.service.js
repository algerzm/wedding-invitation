/* eslint-disable object-curly-newline */
import { initializeApp } from '@firebase/app';
import {
  collection,
  getDocs,
  getFirestore,
  query,
  where,
  updateDoc,
  doc,
  addDoc,
} from '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBj5-UFdMX5n_bWRSBH6K0jPULDvPOpwdQ',
  authDomain: 'wedding-invitation-86049.firebaseapp.com',
  projectId: 'wedding-invitation-86049',
  storageBucket: 'wedding-invitation-86049.appspot.com',
  messagingSenderId: '317235638486',
  appId: '1:317235638486:web:8ec410f439ae68f6c69977',
  measurementId: 'G-3HFFWDV0TC',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const saveAssistance = async (docId, invitation) => {
  try {
    const invRef = doc(db, 'invitations', docId);
    await updateDoc(invRef, invitation);
  } catch (e) {
    return false;
  }

  return true;
};

const getInvitation = async (
  invitationCode,
  newConfirmedGuests,
  fullName,
  invMessage,
  email,
) => {
  let error = false;
  let message = '';
  const invitations = [];
  let docId = null;
  const upperInvitationCode = invitationCode.toUpperCase();

  try {
    const invitationByCode = query(
      collection(db, 'invitations'),
      where('code', '==', upperInvitationCode),
    );
    const docs = await getDocs(invitationByCode);
    if (docs) {
      docs.forEach((docItem) => {
        docId = docItem.id;
        invitations.push(docItem.data());
      });
    }

    if (invitations.length === 0) {
      error = true;
      message = 'No se encontro el codigo de invitacion, favor de verificar.';
    }
  } catch (e) {
    error = true;
    message = 'Ocurrio un error inesperado, favor de reportarlo.';
  }

  if (!error) {
    const invitation = invitations[0];
    const { confirmedGuests, maxGuests } = invitation;

    if (confirmedGuests + Number(newConfirmedGuests) > maxGuests) {
      error = true;
      message = `El codigo de invitacion es solamente para ${maxGuests} invitados, y actualmente se han confirmado ${confirmedGuests} asistente(s), dando un total de ${
        confirmedGuests + Number(newConfirmedGuests)
      }, verifica de nuevo el numero de asistentes a confirmar.`;
    } else {
      invitation.confirmedGuests = confirmedGuests + Number(newConfirmedGuests);
      invitation.guestsNames.push({
        fullName,
        invMessage,
        email,
        confirmed: Number(newConfirmedGuests),
      });
      if (saveAssistance(docId, invitation)) {
        message = `Se han confirmado ${Number(
          newConfirmedGuests,
        )} asistente(s) con exito!`;
      } else {
        error = true;
        message = 'Ocurrio un error al confirmar la asistencia, intente de nuevo.';
      }
    }
  }

  return {
    error,
    message,
    invitations,
  };
};

const addNewCode = async (code, groupDescription, maxGuests) => {
  const invObj = {
    active: true,
    code,
    confirmedGuests: Number(0),
    groupDescription,
    guestsNames: [],
    maxGuests: Number(maxGuests),
  };

  const docRef = await addDoc(collection(db, 'invitations'), invObj);

  return docRef;
};

export default getInvitation;

export { addNewCode };
