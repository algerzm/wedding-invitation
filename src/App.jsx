import { Routes, Route } from 'react-router-dom';

//* Views
import Home from './views/Home/Home.view';
import Invitation from './views/Invitation/Invitation.view';
import NotFound from './views/NotFound/NotFound.view';
// import AddCode from './views/AddCode/AddCode.view';

function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="inv/:invitationName" element={<Invitation />} />
        {/* <Route path="addCode" element={<AddCode />} /> */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
