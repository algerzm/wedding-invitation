/* eslint-disable no-alert */
/* eslint-disable jsx-a11y/label-has-associated-control */
import { useState } from 'react';
import { addNewCode } from '../../services/firebase.service';

const AddCode = function AddCode() {
  const [formValues, setFormValues] = useState({
    code: '',
    groupDescription: '',
    maxGuests: 0,
  });

  const handleChange = (event) => {
    setFormValues({
      ...formValues,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="w-full bg-slate-500 h-screen">
      <h2 className="text-4xl text-center">Agregar Codigo</h2>
      <div className="flex mt-40 justify-center items-center content-center">
        <div className="flex flex-col">
          <label htmlFor="code" className="text-2xl font-bold">
            Code
          </label>
          <input
            type="text"
            className="mr-10 w-60 h-10 text-2xl"
            name="code"
            id="code"
            onChange={handleChange}
            value={formValues.code}
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="groupDescription" className="text-2xl font-bold">
            Descripcion Grupo
          </label>
          <input
            type="text"
            className="mr-10 w-60 h-10 text-2xl"
            name="groupDescription"
            id="groupDescription"
            onChange={handleChange}
            value={formValues.groupDescription}
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="maxGuests" className="text-2xl font-bold">
            Max Invitados
          </label>
          <input
            type="number"
            className="mr-10 w-60 h-10 text-2xl"
            name="maxGuests"
            id="maxGuests"
            onChange={handleChange}
            value={formValues.maxGuests}
          />
        </div>
      </div>
      <div className="flex justify-center items-center">
        <button
          type="button"
          className="bg-blue-500 w-40 mt-20 h-10 rounded-xl font-bold text-2xl hover:bg-blue-800"
          onClick={() => {
            const a = addNewCode(
              formValues.code,
              formValues.groupDescription,
              formValues.maxGuests,
            );

            if (a) {
              alert('Codigo agregado');
              setFormValues({
                code: '',
                groupDescription: '',
                maxGuests: 0,
              });
            }
          }}
        >
          Agregar
        </button>
      </div>
    </div>
  );
};

export default AddCode;
