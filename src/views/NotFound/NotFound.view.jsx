export default function NotFound() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <p>Not Found!</p>
    </div>
  );
}
