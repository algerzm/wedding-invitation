/* eslint-disable operator-linebreak */
//* PropTypes
import PropTypes from 'prop-types';
//* Components
import InfoSectionWrapper from './InfoSectionWrapper.component';
import SeparatedPhotos from './SeparatedPhotos.component';
import InfoSectionText from './InfoSectionText.component';
import EventsInfo from './EventsInfo.component';
import CountdownTimer from './CountdownTimer.component';

const BASE_URL = process.env.REACT_APP_API_URL;

const InfoSection = function InfoSection({
  infoText,
  infoTitle,
  infoBible,
  ceremonyAddress,
  ceremonyTime,
  ceremonyLocation,
  partyAddress,
  partyTime,
  partyLocation,
  targetDate,
  highlightColor,
  bgColor,
  textColor,
  boyfriendImg,
  girlfriendImg,
  titleTextColor,
}) {
  return (
    <InfoSectionWrapper bgColor={bgColor}>
      <SeparatedPhotos
        girlfriendImg={`${BASE_URL}${girlfriendImg}`}
        boyfriendImg={`${BASE_URL}${boyfriendImg}`}
      />
      <InfoSectionText
        title={infoTitle}
        text={infoText}
        textColor={textColor}
        titleTextColor={titleTextColor}
        infoBible={infoBible}
      />
      <EventsInfo
        ceremonyAddress={ceremonyAddress}
        ceremonyTime={ceremonyTime}
        ceremonyLocation={ceremonyLocation}
        partyAddress={partyAddress}
        partyTime={partyTime}
        partyLocation={partyLocation}
        highligtColor={highlightColor}
        textColor={textColor}
        titleTextColor={titleTextColor}
      />
      <CountdownTimer
        targetDate={targetDate}
        highlightColor={highlightColor}
        bgInfoColor={bgColor}
        textColor={textColor}
      />
    </InfoSectionWrapper>
  );
};

InfoSection.propTypes = {
  infoText: PropTypes.string.isRequired,
  infoTitle: PropTypes.string.isRequired,
  infoBible: PropTypes.string.isRequired,
  ceremonyAddress: PropTypes.string.isRequired,
  ceremonyTime: PropTypes.string.isRequired,
  ceremonyLocation: PropTypes.string.isRequired,
  partyAddress: PropTypes.string.isRequired,
  partyTime: PropTypes.string.isRequired,
  partyLocation: PropTypes.string.isRequired,
  targetDate: PropTypes.number.isRequired,
  highlightColor: PropTypes.string.isRequired,
  bgColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  boyfriendImg: PropTypes.string.isRequired,
  girlfriendImg: PropTypes.string.isRequired,
  titleTextColor: PropTypes.string.isRequired,
};

export default InfoSection;
