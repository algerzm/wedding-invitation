/* eslint-disable object-curly-newline */
import PropTypes from 'prop-types';

const BASE_URL = process.env.REACT_APP_API_URL;

const MainBanner = function MainBanner({
  aboveText,
  belowText,
  darkMode,
  imageUrl,
  circleTextColor,
}) {
  return (
    <div
      className="first-image bg-cover bg-center flex justify-center items-center"
      style={{
        backgroundImage: `url(${BASE_URL}${imageUrl})`,
      }}
    >
      <div
        className={`square-width-30 rounded-full ${
          darkMode ? 'circle-inv-dark' : 'circle-inv'
        } mt-80`}
      >
        <div className={darkMode ? 'inner-circle-inv-dark' : 'inner-circle-inv'}>
          <p
            className="text-2xl lg:text-3xl tracking-wide italic font-semibold"
            style={{ color: circleTextColor }}
          >
            {aboveText}
          </p>
          <hr className={`w-3/4 ${darkMode ? 'border-gray-200' : 'text-gray-900'}`} />
          <p
            className="text-2xl lg:text-3xl font-semibold italic"
            style={{ color: circleTextColor }}
          >
            {belowText}
          </p>
        </div>
      </div>
    </div>
  );
};

MainBanner.propTypes = {
  aboveText: PropTypes.string.isRequired,
  belowText: PropTypes.string.isRequired,
  darkMode: PropTypes.bool,
  imageUrl: PropTypes.string.isRequired,
  circleTextColor: PropTypes.string.isRequired,
};

MainBanner.defaultProps = {
  darkMode: false,
};

export default MainBanner;
