import { useNavigate } from 'react-router-dom';

const FloatingActionButton = function FloatingActionButton() {
  const navigate = useNavigate();

  return (
    <div className="fixed right-8 bottom-8">
      <button
        type="button"
        className="text-gray-800 text-lg font-bold px-4 z-[100] w-auto h-10 bg-slate-100 rounded-full hover:bg-slate-300 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none"
        onClick={() => navigate('/')}
      >
        <svg
          viewBox="0 0 20 20"
          enableBackground="new 0 0 20 20"
          className="w-5 h-5 inline-block mr-2 mb-2"
        >
          <path
            fill="#1f2937"
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M11 19l-7-7 7-7m8 14l-7-7 7-7"
          />
        </svg>
        <span>Inicio</span>
      </button>
    </div>
  );
};

export default FloatingActionButton;
