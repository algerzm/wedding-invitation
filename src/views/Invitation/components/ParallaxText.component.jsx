/* eslint-disable object-curly-newline */
import PropTypes from 'prop-types';
//* Parallax
import { Parallax } from 'react-parallax';

const BASE_URL = process.env.REACT_APP_API_URL;

const ParallaxText = function ParallaxText({ title, text, img, textColor }) {
  return (
    <Parallax
      blur={{
        min: -5,
        max: 10,
      }}
      strength={200}
      bgImage={`${BASE_URL}${img}`}
      bgImageAlt="wedding"
      className="w-full h-auto"
      bgClassName="object-cover object-center"
    >
      <div className="w-full flex justify-start text-center gap-14 flex-col p-10 py-40">
        <p
          className="font-wedding text-6xl font-bold"
          style={{
            color: textColor,
          }}
        >
          {title}
        </p>
        <div className="w-full flex justify-center">
          <p
            className="w-full md:w-2/4 lg:w-1/3 text-lg font-normal font-relaway italic p-4 rounded-2xl"
            style={{
              color: textColor,
            }}
          >
            {text}
          </p>
        </div>
      </div>
    </Parallax>
  );
};

ParallaxText.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
};

export default ParallaxText;
