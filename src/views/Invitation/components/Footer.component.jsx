import PropTypes from 'prop-types';

const Footer = function Footer({ bgColor, textColor }) {
  return (
    <footer
      className="w-full font-relaway italic h-8 flex justify-center items-center gap-2 text-center py-12 text-lg font-medium"
      style={{
        backgroundColor: bgColor,
        color: textColor,
      }}
    >
      <p>Designed by.</p>
      <p>Alger Ivan Z.</p>
    </footer>
  );
};

Footer.propTypes = {
  bgColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
};

export default Footer;
