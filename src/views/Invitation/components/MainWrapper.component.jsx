import PropTypes from 'prop-types';

const MainWrapper = function MainWrapper({ children }) {
  return <div className="w-full">{children}</div>;
};

MainWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainWrapper;
