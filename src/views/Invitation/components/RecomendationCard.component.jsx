import PropTypes from 'prop-types';

const RecomendationCard = function RecomendationCard({
  title,
  description,
  cardColor,
  textColor,
}) {
  return (
    <article
      className="w-full md:w-1/4 flex text-center flex-col shadow-lg"
      style={{
        backgroundColor: cardColor,
        color: textColor,
      }}
    >
      <div className="h-1/4 pt-8 md:mb-8">
        <p className="text-2xl font-relaway italic font-bold">{title}</p>
      </div>
      <p className="px-4 font-relaway mb-8">{description}</p>
    </article>
  );
};

RecomendationCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  cardColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
};

export default RecomendationCard;
