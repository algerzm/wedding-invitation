import PropTypes from 'prop-types';
import styled from 'styled-components';
//* Slider
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
//* Components
import GalleryImage from './GalleryImage.component';

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,
  },
};

const GalleryContainer = styled.section`
  background-color: ${(props) => props.bgColor};
`;

const BASE_URL = process.env.REACT_APP_API_URL;

const GallerySection = function GallerySection({
  decorationColor,
  bgColor,
  textColor,
  images,
}) {
  return (
    <GalleryContainer className="gallery-container pb-24 shadow-inner" bgColor={bgColor}>
      <div className="w-full flex justify-center items-center">
        <h1
          className="font-wedding text-4xl md:text-5xl py-6 mt-4 font-bold"
          style={{ color: textColor }}
        >
          Galeria de Fotos
        </h1>
      </div>
      <div className="pt-12 px-1">
        <Carousel
          infinite
          responsive={responsive}
          autoPlay
          autoPlaySpeed={3500}
          itemClass="mr-4"
        >
          {images.map((image, index) => (
            <GalleryImage
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              image={`${BASE_URL}${image}`}
              altTags="ivan"
              decorationColor={decorationColor}
            />
          ))}
        </Carousel>
      </div>
    </GalleryContainer>
  );
};

GallerySection.propTypes = {
  decorationColor: PropTypes.string.isRequired,
  bgColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default GallerySection;
