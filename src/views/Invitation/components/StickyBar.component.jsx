import PropTypes from 'prop-types';

const StickyBar = function StickyBar({ title, bgColor, textColor }) {
  return (
    <div
      className="w-full sticky h-16 top-0 py-3 flex justify-center items-center z-50"
      style={{ backgroundColor: bgColor }}
    >
      <p
        className="font-wedding font-bold text-4xl tracking-wider"
        style={{
          color: textColor,
        }}
      >
        {title}
      </p>
    </div>
  );
};

StickyBar.propTypes = {
  title: PropTypes.string.isRequired,
  bgColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
};

export default StickyBar;
