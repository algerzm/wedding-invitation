import PropTypes from 'prop-types';
//* Components
import RecomendationCard from './RecomendationCard.component';

const RecomendationsSection = function RecomendationsSection({
  bgColor,
  textColor,
  cardColor,
  cardTextColor,
  cardsItems,
}) {
  return (
    <section
      className="flex justify-center items-center flex-col py-16"
      style={{
        backgroundColor: bgColor,
      }}
    >
      <p
        className="text-3xl md:text-5xl font-bold font-wedding"
        style={{
          color: textColor,
        }}
      >
        Recomendaciones
      </p>
      <div className="w-full flex flex-col md:flex-row justify-between gap-4 mt-6 mb-6 px-6">
        {cardsItems.map((item) => (
          <RecomendationCard
            key={item.id}
            title={item.title}
            description={item.description}
            cardColor={cardColor}
            textColor={cardTextColor}
          />
        ))}
      </div>
    </section>
  );
};

RecomendationsSection.propTypes = {
  bgColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  cardColor: PropTypes.string.isRequired,
  cardTextColor: PropTypes.string.isRequired,
  cardsItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
};

export default RecomendationsSection;
