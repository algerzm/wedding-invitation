/* eslint-disable operator-linebreak */
import Swal from 'sweetalert2';
import { useState } from 'react';
import PropTypes from 'prop-types';
import getInvitation from '../../../services/firebase.service';
//* Components
import InputText from '../../../components/InputText';
import InputTextArea from '../../../components/InputTextArea';
import DropdownLabeled from '../../../components/DropdownLabeled';

const AssistanceForm = function AssistanceForm({
  boyfriend,
  girlfriend,
  bgColor,
  highlightColor,
  formColor,
}) {
  const [formValues, setFormValues] = useState({
    email: '',
    fullName: '',
    invitationCode: '',
    numberOfGuests: 0,
    message: '',
  });

  const handleChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (
      !formValues.email ||
      !formValues.fullName ||
      !formValues.invitationCode ||
      !formValues.numberOfGuests
    ) {
      Swal.fire({
        title: 'Ups...',
        text: 'Por favor, llena todos los campos para continuar',
        icon: 'error',
        confirmButtonText: 'Aceptar',
        confirmButtonColor: highlightColor,
        iconColor: highlightColor,
      });
      return;
    }

    const { error, message } = await getInvitation(
      formValues.invitationCode,
      formValues.numberOfGuests,
      formValues.fullName,
      formValues.message,
      formValues.email,
    );

    Swal.fire({
      title: error ? 'Ups!' : 'Gracias!',
      text: message,
      icon: error ? 'error' : 'success',
      confirmButtonText: 'Aceptar',
      confirmButtonColor: highlightColor,
      iconColor: highlightColor,
    });
  };

  return (
    <div
      className="flex py-24 justify-center items-center"
      style={{
        backgroundColor: bgColor,
      }}
    >
      <div
        className="w-3/4 p-8 shadow-md md:shadow-2xl"
        style={{
          backgroundColor: formColor,
        }}
      >
        <p className="text-center mb-8 font-wedding text-4xl font-bold">
          Confirmar Asistencia
        </p>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-8 gap-y-4">
          <InputText name="email" label="E-Mail" handleChange={handleChange} />
          <InputText
            name="fullName"
            label="Nombre Completo"
            handleChange={handleChange}
          />
          <InputText
            name="invitationCode"
            label="Codigo de Invitacion"
            handleChange={handleChange}
          />
          <DropdownLabeled
            label="Numero de asistentes:"
            handleChange={handleChange}
            name="numberOfGuests"
          />
          <InputTextArea
            className="md:col-span-2"
            name="message"
            label={`Mensaje para ${girlfriend} y ${boyfriend}`}
            handleChange={handleChange}
          />
          <div className="flex justify-end mt-4 md:col-span-2">
            <button
              type="button"
              className="w-48 h-10 rounded-3xl text-white font-relaway italic text-lg"
              style={{
                backgroundColor: highlightColor,
              }}
              onClick={handleSubmit}
            >
              Confirmar Asistencia
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

AssistanceForm.propTypes = {
  boyfriend: PropTypes.string.isRequired,
  girlfriend: PropTypes.string.isRequired,
  bgColor: PropTypes.string.isRequired,
  highlightColor: PropTypes.string.isRequired,
  formColor: PropTypes.string.isRequired,
};

export default AssistanceForm;
