import PropTypes from 'prop-types';
import styled from 'styled-components';

//* Styles
import '../styles/Invitation.styles.css';
//* Hooks
import useCountdown from '../hooks/useCountdown';

const Wrapper = styled.div`
  border: ${(props) => props.border};
  color: ${(props) => props.textColor};
  &::before {
    background: ${(props) => props.bgInfoColor};
    color: ${(props) => props.textColor};
  }
`;

const TimerDiv = styled.div`
  color: ${(props) => props.textColor};
  &::before {
    color: ${(props) => props.textColor};
  }
`;

const CountdownTimer = function CountdownTimer({
  targetDate,
  highlightColor,
  bgInfoColor,
  textColor,
}) {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);

  const border = `1px solid ${highlightColor}`;

  return (
    <Wrapper
      className="counter-container"
      border={border}
      bgInfoColor={bgInfoColor}
      textColor={textColor}
    >
      <TimerDiv textColor={textColor}>{days}</TimerDiv>
      <TimerDiv textColor={textColor}>{hours}</TimerDiv>
      <TimerDiv textColor={textColor}>{minutes}</TimerDiv>
      <TimerDiv textColor={textColor}>{seconds}</TimerDiv>
    </Wrapper>
  );
};

CountdownTimer.propTypes = {
  targetDate: PropTypes.number.isRequired,
  highlightColor: PropTypes.string.isRequired,
  bgInfoColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
};

export default CountdownTimer;
