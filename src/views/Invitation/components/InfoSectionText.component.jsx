import PropTypes from 'prop-types';

const InfoSectionText = function InfoSectionText({
  title,
  text,
  textColor,
  titleTextColor,
  infoBible,
}) {
  return (
    <div className="mt-8 lg:-mt-64 text-center w-full flex justify-center flex-col">
      <div
        className="flex justify-center mb-10 font-wedding text-5xl font-bold"
        style={{
          color: titleTextColor,
        }}
      >
        <p>{title}</p>
      </div>
      <div className="flex justify-center">
        <p
          className="text-center text-gray-800 lg:w-1/3 font-relaway italic text-xl"
          style={{
            color: textColor,
          }}
        >
          {text}
        </p>
      </div>
      <p
        className="text-center text-gray-800 font-relaway italic text-xl pt-4"
        style={{
          color: textColor,
        }}
      >
        {infoBible}
      </p>
    </div>
  );
};

InfoSectionText.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  titleTextColor: PropTypes.string.isRequired,
  infoBible: PropTypes.string.isRequired,
};

export default InfoSectionText;
