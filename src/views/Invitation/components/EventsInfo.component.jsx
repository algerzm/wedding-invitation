import PropTypes from 'prop-types';

const EventsInfo = function EventInfo({
  ceremonyAddress,
  ceremonyTime,
  ceremonyLocation,
  partyAddress,
  partyTime,
  partyLocation,
  highligtColor,
  textColor,
  titleTextColor,
}) {
  return (
    <div
      className="flex flex-col md:flex-row w-full mt-8 lg:mt-28"
      style={{
        color: textColor,
      }}
    >
      <div className="w-full md:w-1/2 flex justify-center flex-col">
        <p
          className="font-wedding text-3xl text-center"
          style={{
            color: titleTextColor,
          }}
        >
          Ceremonia
        </p>
        <p
          className="text-center py-4 px-8 md:px-14 mb-4"
          style={{
            color: textColor,
          }}
        >
          {ceremonyAddress}
        </p>
        <div className="w-full flex justify-center gap-12">
          <button
            type="button"
            className="w-32 h-10 border-2 rounded-3xl font-relaway italic text-lg hover:cursor-default"
            style={{
              borderColor: highligtColor,
              color: highligtColor,
            }}
          >
            {ceremonyTime}
          </button>
          <button
            type="button"
            className="w-32 h-10 rounded-3xl text-white font-relaway italic text-lg"
            style={{
              backgroundColor: highligtColor,
            }}
            onClick={() => window.open(ceremonyLocation, '_blank')}
          >
            Ubicacion
          </button>
        </div>
      </div>
      <div
        className="divide-y-8 h-40 border-l-2 hidden md:block mb-6"
        style={{
          borderColor: highligtColor,
        }}
      />
      <div
        className="border-t-2 block md:hidden m-10"
        style={{
          borderColor: highligtColor,
        }}
      />
      <div className="w-full md:w-1/2 flex justify-center flex-col">
        <p
          className="font-wedding text-3xl text-center"
          style={{
            color: titleTextColor,
          }}
        >
          Fiesta
        </p>
        <p
          className="text-center py-4 px-8 md:px-14 mb-4"
          style={{
            color: textColor,
          }}
        >
          {partyAddress}
        </p>
        <div className="w-full flex justify-center gap-12">
          <button
            type="button"
            className="w-32 h-10 border-2 rounded-3xl font-relaway italic text-lg hover:cursor-default"
            style={{
              borderColor: highligtColor,
              color: highligtColor,
            }}
          >
            {partyTime}
          </button>
          <button
            type="button"
            className="w-32 h-10 rounded-3xl text-white font-relaway italic text-lg"
            style={{
              backgroundColor: highligtColor,
            }}
            onClick={() => window.open(partyLocation, '_blank')}
          >
            Ubicacion
          </button>
        </div>
      </div>
    </div>
  );
};

EventsInfo.propTypes = {
  ceremonyAddress: PropTypes.string.isRequired,
  ceremonyTime: PropTypes.string.isRequired,
  ceremonyLocation: PropTypes.string.isRequired,
  partyAddress: PropTypes.string.isRequired,
  partyTime: PropTypes.string.isRequired,
  partyLocation: PropTypes.string.isRequired,
  highligtColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  titleTextColor: PropTypes.string.isRequired,
};

export default EventsInfo;
