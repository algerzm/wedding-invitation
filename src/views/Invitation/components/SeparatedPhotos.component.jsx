import PropTypes from 'prop-types';

const SeparatedPhotos = function SeparatedPhotos({ girlfriendImg, boyfriendImg }) {
  return (
    <div className="flex justify-between content-start">
      <div
        className="w-80 h-80 bg-cover bg-center md:rounded-full rounded-l-md"
        style={{
          backgroundImage: `url(${boyfriendImg})`,
        }}
      />
      <div
        className="w-80 h-80 bg-cover bg-center md:rounded-full rounded-r-md"
        style={{
          backgroundImage: `url(${girlfriendImg})`,
        }}
      />
    </div>
  );
};

SeparatedPhotos.propTypes = {
  girlfriendImg: PropTypes.string.isRequired,
  boyfriendImg: PropTypes.string.isRequired,
};

export default SeparatedPhotos;
