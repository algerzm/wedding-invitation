import PropTypes from 'prop-types';
import styled from 'styled-components';

const ImageItem = styled.img`
  outline: 3px solid ${(props) => props.decorationColor};
  border: 5px solid ${(props) => props.decorationColor};
  @media (max-width: 400px) {
    outline: 1px solid ${(props) => props.decorationColor};
    border: 3px solid ${(props) => props.decorationColor};
  }
`;

const GalleryImage = function GalleryImage({ image, altTags, decorationColor }) {
  return (
    <ImageItem
      src={image}
      className="slider-img"
      alt={altTags}
      loading="lazy"
      decorationColor={decorationColor}
    />
  );
};

GalleryImage.propTypes = {
  image: PropTypes.string.isRequired,
  altTags: PropTypes.string.isRequired,
  decorationColor: PropTypes.string.isRequired,
};

export default GalleryImage;
