import PropTypes from 'prop-types';

const InfoSectionWrapper = function InfoSectionWrapper({ children, bgColor }) {
  return (
    <section
      className="min-h-screen w-full flex justify-center p-14 px-8 md:px-24"
      style={{
        backgroundColor: bgColor,
      }}
    >
      <div className="w-full flex flex-col">{children}</div>
    </section>
  );
};

InfoSectionWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  bgColor: PropTypes.string.isRequired,
};

export default InfoSectionWrapper;
