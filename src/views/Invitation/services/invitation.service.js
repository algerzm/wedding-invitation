import axios from 'axios';

const BASE_URL = `${process.env.REACT_APP_API_URL}/api`;

const getInvitationById = function getInvitationById(invitationId) {
  return axios.get(`${BASE_URL}/invitations/${invitationId}?populate=*`);
};

const getAnything = function getAnything() {
  return 'hello';
};

export { getInvitationById, getAnything };
