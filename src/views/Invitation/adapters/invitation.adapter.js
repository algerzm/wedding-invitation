const createAdaptedInvitation = (invitationResponse) => {
  const dt = new Date(invitationResponse.weddingDate);
  const targetDate = dt.getTime();

  const dataColors = invitationResponse.invitation_color.data.attributes;
  const dataRecomendations = invitationResponse.recomendations.data;

  const recomendationItems = dataRecomendations.map((recomendation) => ({
    id: recomendation.id,
    title: recomendation.attributes.title,
    description: recomendation.attributes.description,
  }));

  const images = invitationResponse.galleryImages.data.map(
    (image) => image.attributes.url,
  );

  return {
    boyfriendFirstName: invitationResponse.boyfriendName.split(' ')[0],
    girlfriendFirstName: invitationResponse.girlfriendName.split(' ')[0],
    boyfriendName: invitationResponse.boyfriendName,
    girlfriendName: invitationResponse.girlfriendName,
    circleTitle: invitationResponse.circleTitle,
    circleSubtitle: invitationResponse.circleSubtitle,
    infoText: invitationResponse.infoText,
    infoTitle: invitationResponse.infoTitle,
    infoBible: invitationResponse.infoSectionBible,
    ceremonyAddress: invitationResponse.ceremonyAddress,
    ceremonyTime: invitationResponse.ceremonyTime,
    ceremonyLocationUrl: invitationResponse.ceremonyLocationUrl,
    partyAddress: invitationResponse.partyAddress,
    partyTime: invitationResponse.partyTime,
    partyLocationUrl: invitationResponse.partyLocationUrl,
    weddingDate: targetDate,
    parallaxTitle: invitationResponse.parallaxTitle,
    parallaxText: invitationResponse.parallaxText,
    colors: {
      stickyBarColor: dataColors.stickyBarColor,
      stickyBarTextColor: dataColors.stickyBarTextColor,
      darkCircle: dataColors.darkCircle,
      circleTextColor: dataColors.circleTextColor,
      highlightColor: dataColors.highlightColor,
      bgInfoSectionColor: dataColors.infoSectionColor,
      infoSectionTextColor: dataColors.infoSectionTextColor,
      infoSectionTitleTextColor: dataColors.infoSectionTitleTextColor,
      parallaxTextColor: dataColors.parallaxTextColor,
      assistanceFormBgColor: dataColors.bgFormColor,
      formColor: dataColors.formColor,
      galleryDecorationColor: dataColors.galleryImageDecoratorColor,
      galleryBgColor: dataColors.galleryBgColor,
      galleryTextColor: dataColors.galleryTextColor,
      recomendationsBgColor: dataColors.recomendationsBgColor,
      recomendationsTextColor: dataColors.recomendationTextColor,
      recomendationsCardColor: dataColors.recomendationsCardColor,
      recomendationsCardTextColor: dataColors.recomendationCardTextColor,
      footerBgColor: dataColors.footerBgColor,
      footerTextColor: dataColors.footerTextColor,
    },
    recomendations: recomendationItems,
    mainBannerImage: invitationResponse.mainBannerImage.data.attributes.url,
    parallaxImage: invitationResponse.parallaxImage.data.attributes.url,
    boyfriendImage: invitationResponse.boyfriendAloneImage.data.attributes.url,
    girlfriendImage: invitationResponse.girlfriendAloneImage.data.attributes.url,
    galleryImages: images,
  };
};

export default createAdaptedInvitation;
