/* eslint-disable operator-linebreak */
import React, { useEffect, useState, Suspense } from 'react';
import './styles/Invitation.styles.css';
//* Parallax
import { useParams } from 'react-router-dom';
//* Services
import { getInvitationById } from './services/invitation.service';
import createAdaptedInvitation from './adapters/invitation.adapter';
//* Components
import MainWrapper from './components/MainWrapper.component';
import StickyBar from './components/StickyBar.component';
import MainBanner from './components/MainBanner.component';
import InfoSection from './components/InfoSection.component';
import ParallaxText from './components/ParallaxText.component';
import AssistanceForm from './components/AssistanceForm.component';
// import GallerySection from './components/GallerySection.component';
import RecomendationsSection from './components/RecomendationsSection.component';
import FloatingActionButton from './components/FloatingActionButton.component';
import Footer from './components/Footer.component';

const GallerySection = React.lazy(() => import('./components/GallerySection.component'));

export default function Invitation() {
  const [invitation, setInvitation] = useState({});
  const { invitationName } = useParams();
  const invitationId = invitationName === 'genesisyalger' ? 1 : 2;

  useEffect(() => {
    const getInvitation = async () => {
      const response = await getInvitationById(invitationId);
      if (response.status === 200) {
        setInvitation(createAdaptedInvitation(response.data.data.attributes));
      }
    };

    getInvitation();
  }, []);

  return invitation.boyfriendFirstName ? (
    <MainWrapper>
      <FloatingActionButton />
      <StickyBar
        title={`${invitation.girlfriendFirstName} y ${invitation.boyfriendFirstName}`}
        bgColor={invitation.colors.stickyBarColor}
        textColor={invitation.colors.stickyBarTextColor}
      />
      <MainBanner
        aboveText={invitation.circleTitle}
        belowText={invitation.circleSubtitle}
        darkMode={invitation.colors.darkCircle}
        imageUrl={invitation.mainBannerImage}
        circleTextColor={invitation.colors.circleTextColor}
      />
      <InfoSection
        infoText={invitation.infoText}
        infoTitle={invitation.infoTitle}
        infoBible={invitation.infoBible}
        ceremonyAddress={invitation.ceremonyAddress}
        ceremonyTime={invitation.ceremonyTime}
        ceremonyLocation={invitation.ceremonyLocationUrl}
        partyAddress={invitation.partyAddress}
        partyTime={invitation.partyTime}
        partyLocation={invitation.partyLocationUrl}
        targetDate={invitation.weddingDate}
        highlightColor={invitation.colors.highlightColor}
        bgColor={invitation.colors.bgInfoSectionColor}
        textColor={invitation.colors.infoSectionTextColor}
        boyfriendImg={invitation.boyfriendImage}
        girlfriendImg={invitation.girlfriendImage}
        titleTextColor={invitation.colors.infoSectionTitleTextColor}
      />
      <ParallaxText
        title={invitation.parallaxTitle}
        text={invitation.parallaxText}
        img={invitation.parallaxImage}
        textColor={invitation.colors.parallaxTextColor}
      />
      <AssistanceForm
        formColor={invitation.colors.formColor}
        boyfriend={invitation.boyfriendFirstName}
        girlfriend={invitation.girlfriendFirstName}
        bgColor={invitation.colors.assistanceFormBgColor}
        highlightColor={invitation.colors.highlightColor}
      />
      <Suspense fallback={<div>loading...</div>}>
        <GallerySection
          decorationColor={invitation.colors.galleryDecorationColor}
          bgColor={invitation.colors.galleryBgColor}
          textColor={invitation.colors.galleryTextColor}
          images={invitation.galleryImages}
        />
      </Suspense>
      <RecomendationsSection
        bgColor={invitation.colors.recomendationsBgColor}
        textColor={invitation.colors.recomendationsTextColor}
        cardColor={invitation.colors.recomendationsCardColor}
        cardTextColor={invitation.colors.recomendationsCardTextColor}
        cardsItems={invitation.recomendations}
      />
      <Footer
        bgColor={invitation.colors.footerBgColor}
        textColor={invitation.colors.footerTextColor}
      />
    </MainWrapper>
  ) : (
    <div>Loading...</div>
  );
}
