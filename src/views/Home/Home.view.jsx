import { useState } from 'react';
//* Components
import CircleNames from './components/CircleNames';

const ALGER_IMAGE = 'https://wcms.algerzm.me/uploads/IMG_4989_2e36d02fea.JPEG';
const KARLA_IMAGE = 'https://wcms.algerzm.me/uploads/boton_dda1f36d44.jpg';

export default function Home() {
  const [isHovered, setIsHovered] = useState({
    genesisyalger: false,
    karlayalberto: false,
  });

  const onHoverFn = (invName) => {
    setIsHovered({
      ...isHovered,
      [invName]: true,
    });
  };

  const onLeaveFn = (invName) => {
    setIsHovered({
      ...isHovered,
      [invName]: false,
    });
  };
  return (
    <div className="h-screen w-screen max-h-screen bg-slate-400 flex flex-col md:flex-row">
      <div
        className={`w-full md:w-1/2 h-1/2 md:h-screen bg-cover bg-center flex items-center justify-center ${
          !isHovered.genesisyalger && 'grayscale'
        }`}
        style={{
          backgroundImage: `url(${ALGER_IMAGE})`,
        }}
      >
        <CircleNames
          onHover={onHoverFn}
          onLeave={onLeaveFn}
          invName="genesisyalger"
          boyfriend="Alger"
          girlfriend="Génesis"
        />
      </div>
      <div
        className={`w-full md:w-1/2 h-1/2 md:h-screen bg-cover bg-center flex items-center justify-center ${
          !isHovered.karlayalberto && 'grayscale'
        }`}
        style={{
          backgroundImage: `url(${KARLA_IMAGE})`,
        }}
      >
        <CircleNames
          onHover={onHoverFn}
          onLeave={onLeaveFn}
          invName="karlayalberto"
          boyfriend="Alberto"
          girlfriend="Karla"
        />
      </div>
    </div>
  );
}
