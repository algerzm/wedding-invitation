import '../styles/CircleNames.css';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';

const CircleNames = function CircleNames({
  onHover,
  onLeave,
  invName,
  boyfriend,
  girlfriend,
}) {
  const navigate = useNavigate();

  return (
    <div
      role="button"
      onMouseEnter={() => onHover(invName)}
      onMouseLeave={() => onLeave(invName)}
      onClick={() => navigate(`/inv/${invName}`)}
      className="circle"
      aria-hidden
    >
      <div className="circle-letters text-center">
        <p className="h-12 flex justify-center items-center content-center">
          {girlfriend}
        </p>
        <p className="h-12 flex justify-center items-center content-center">&</p>
        <p className="h-12 flex justify-center items-center content-center">
          {boyfriend}
        </p>
      </div>
    </div>
  );
};

CircleNames.propTypes = {
  onHover: PropTypes.func.isRequired,
  onLeave: PropTypes.func.isRequired,
  invName: PropTypes.string.isRequired,
  boyfriend: PropTypes.string.isRequired,
  girlfriend: PropTypes.string.isRequired,
};

export default CircleNames;
