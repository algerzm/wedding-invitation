module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      backgroundImage: {
        'homebutton-karla':
          "url('https://images.squarespace-cdn.com/content/v1/512e63d6e4b0c046ed792a96/1583408232189-CR2RQ25PSP197B2MWWO5/Best_Wedding_Photographer_in+_Tuscany.jpg')",
        'homebutton-alger':
          "url('https://assets.vogue.com/photos/6216853d0b9b0c08699edf7f/1:1/w_2000,h_2000,c_limit/AlexandraKnight_LouisBreunerWedding_VOGUE_45.jpg')",
        'main-karla':
          "url('https://images.squarespace-cdn.com/content/v1/512e63d6e4b0c046ed792a96/1583408232189-CR2RQ25PSP197B2MWWO5/Best_Wedding_Photographer_in+_Tuscany.jpg')",
        'main-alger': "url('https://navizm.com/images/boda/bg_main_alger.jpg')",
        'alone-gesys': "url('https://navizm.com/images/boda/gesys3.jpg')",
        'alone-alger': "url('https://navizm.com/images/boda/ivan.jpeg')",
      },
      colors: {
        brown: '#B3947F',
      },
      height: {
        128: '52rem',
      },
      borderWidth: {
        1: '1px',
      },
    },
  },
  plugins: [],
};
